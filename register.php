<?php
include('./src/actions/redirectIfAuthenticated.php');

$pageTitle = 'Página de Cadastro';
?>

<!DOCTYPE html>
<html lang="en">
<?php include("./src/components/header.php") ?>

<body>
  <?php include("./src/components/navbar.php") ?>
  
  <section class="hero is-primary">
    <div class="hero-body">
      <div class="container is-fluid">
        <h1 class="title">
          Cadastro
        </h1>
      </div>
    </div>
  </section>
  
  <?php include("./src/components/formError.php") ?>
  
  <form class="hero" method="post" action="./src/actions/register.php">
    <div class="hero-body">
      <div class="container">
        <div class="columns">
          <div class="column is-8 is-offset-2">
            <div class="login-form">
              <p class="control has-icon has-icon-right">
                <label class="label">Nome:</label>
                <input class="input email-input" placeholder="Digite seu nome" type="text" name="userName" required>
                <span class="icon user">
                  <i class="fa fa-user-circle"></i>
                </span>
              </p>
              
              <p class="control has-icon has-icon-right">
                <label class="label">E-mail:</label>
                <input class="input email-input" placeholder="nome@dominio.com" type="text" name="userEmail" required>
                <span class="icon user">
                  <i class="fa fa-user"></i>
                </span>
              </p>
              
              <p class="control has-icon has-icon-right">
                <label class="label">Senha:</label>
                <input class="input password-input" placeholder="Digite sua senha" type="password" name="userPassword" required />
                <span class="icon user">
                  <i class="fa fa-lock"></i>
                </span>
              </p>
              
              <p class="control has-icon has-icon-right">
                <label class="label">Repita a senha:</label>
                <input class="input password-input" placeholder="Digite sua senha" type="password" name="userPasswordRepeated" required />
                <span class="icon user">
                  <i class="fa fa-lock"></i>
                </span>
              </p>
              
              <p class="control has-icon has-icon-right">
                <label class="label">Perfil:</label>
                <span class="select is-fullwidth">
                  <select name="userProfile" required>
                    <option disabled selected>Selecione o perfil</option>
                    <option valude="Usuário">Usuário</option>
                    <option value="Prestador">Prestador</option>
                  </select>
                </span>
                <span class="icon user">
                  <i class="fa fa-question"></i>
                </span>
              </p>
              
              <hr />
              
              <p class="control">
                <label class="checkbox">
                  <input type="checkbox" name="agreeTerms" required>
                  <a target="_blank" href="./use-terms.html">Concordo com os termos de uso.</a>
                </label>
              </p>
              
              <p class="control login">
                <button class="button is-primary is-outlined is-large is-fullwidth" button="submit">Enviar</button>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</body>
</html>