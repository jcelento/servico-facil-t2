<?php
/*
Checa se a página possui um título declarado externamente
- Se existir ela concatena com o | (pipe)
- Se não, ela mostra somente o título do sistem
*/
if($pageTitle) {
  $pageTitle = $pageTitle . ' | ';
} else {
  $pageTitle = '';
}
?>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> <?php echo $pageTitle; ?> Serviço Fácil</title>
  <link rel="stylesheet" href="./assets/css/bulma.css " />
  <link rel="stylesheet" href="./assets/css/font-awesome.css " />
  <script src="./assets/js/jquery-3.1.1.min.js "></script>
</head>