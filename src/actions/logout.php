<?php
/*
Invalida qualquer cookie de atutenticação na sessão do usuário
*/
unset($_COOKIE['isAuthenticated']);
unset($_COOKIE['userProfile']);
unset($_COOKIE['userToken']);

setcookie('isAuthenticated', null, -1, '/');
setcookie('userProfile', null, -1, '/');
setcookie('userToken', null, -1, '/');

// Redirect para a página inicial
header('Location: ' . '../../index.php');
?>