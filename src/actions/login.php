<?php
function redirect_to_login() {
  header('Location: ' . '../../login.php?error=true');
  exit();
}

function test_user_existance($email, $password) {
 require_once("db.php");

    $query = "SELECT userProfile, userToken FROM user WHERE userEmail = '$email' AND userPassword = '$password'" ;
    $result = mysqli_query($conn,$query);
    if (mysqli_num_rows($result)!=0)
    {
        $row = mysqli_fetch_row($result);

        $userToken = $row[1];

        setcookie('isAuthenticated', true, 0, '/');
        setcookie('userProfile', $row[0], 0, '/');
        setcookie('userToken', $userToken, 0, '/');

        return true;
    }
    return false;
}

$userEmail = $_POST['userEmail'];
$userPassword = $_POST['userPassword'];

if(empty($userEmail) || empty($userPassword)) {
  redirect_to_login();
}

$userIsValid = test_user_existance(
  $userEmail,
  md5($userPassword)
);

if(!$userIsValid) {
  return redirect_to_login();
}

// Redirect para a página inicial
header('Location: ' . '../../index.php');
?>
