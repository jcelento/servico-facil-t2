<?php
function redirect_to_serviceRegister() {
  header('Location: ' . '../../serviceRegister.php?error=true');
  exit();
}

function save_service($name, $type, $value) {
  require_once("db.php");
  
  $token = $_COOKIE['userToken'];
  
  require_once("db.php");
  $query = "SELECT userId FROM user WHERE userToken = '$token'" ;
  
  $result = mysqli_query($conn,$query);
  $row = mysqli_fetch_array($result);
  $userId = $row[0];
  
  $sql = "INSERT INTO service (serviceName, serviceType, serviceValue, userId) VALUES ('$name','$type','$value', '$userId')";
  
  if (mysqli_query($conn, $sql)) {
    return true;
  }
  else {
    return false;
  }
  
  mysqli_close($conn);
  
  
  return true;
}

$serviceName = $_POST['serviceName'];

if(array_key_exists('choosedService', $_POST)) {
  $serviceType = $_POST['choosedService'];
}

// Prioridade para o tipo que o cliente cadastrar
if(array_key_exists('otherService', $_POST)) {
  $serviceType = $_POST['otherService'];
}

$serviceValue = $_POST['serviceValue'];

if(
  empty($serviceName) || 
  empty($serviceType) ||
  !is_numeric($serviceValue)) {
    redirect_to_serviceRegister();
  }
  
  $userIsValid = save_service(
    $serviceName,
    $serviceType,
    ((float) $serviceValue) // Lisp Like
  );
  
  if(!$userIsValid) {
    return redirect_to_serviceRegister();
  }
  
  // Redirect para a página inicial
  header('Location: ' . '../../index.php');
  ?>