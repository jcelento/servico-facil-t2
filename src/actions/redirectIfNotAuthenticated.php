<?php
// Redireciona para a Página principal se o usuário não estiver autenticado
$isAuthenticated = !empty($_COOKIE['isAuthenticated']);

if(!$isAuthenticated) {
  header('Location: ' . './index.php');
};
?>