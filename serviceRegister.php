<?php
include('./src/actions/redirectIfNotAuthenticated.php');
include('./src/actions/redirectIfNotProvider.php');

$pageTitle = 'Cadastro de serviços';
// Trocar para userId
$userToken = $_COOKIE['userToken'];

require_once("./src/actions/db.php");
require_once("./src/actions/get_types.php");
?>

<!DOCTYPE html>
<html lang="en">
<?php include("./src/components/header.php") ?>

<body>
  <?php include("./src/components/navbar.php") ?>
  
  <section class="hero is-primary">
    <div class="hero-body">
      <div class="container is-fluid">
        <h1 class="title">
          Cadastro de serviços
        </h1>
      </div>
    </div>
  </section>
  
  <?php include("./src/components/formError.php") ?>
  
  <form class="hero" method="post" action="./src/actions/serviceRegister.php">
    <div class="hero-body">
      <div class="container">
        <div class="columns">
          <div class="column is-8 is-offset-2">
            <div class="service-form">
              <input class="input" type="text" name="userToken" value=<?php echo $userToken ?> hidden style="display: none" />
              
              <p class="control">
                <label class="label">Nome do serviço:</label>
                <input class="input" type="text" name="serviceName" required />
              </p>
              
              <p class="control">
                <label class="label">Tipo de serviço:</label>
                <span class="select is-fullwidth">
                  <select name="choosedService">
                    <option disabled selected>Selecione Um serviço</option>
                    <?php foreach($services as $value): ?>
                      <option value=<?php echo $value ?>> <?php echo $value ?> </option>
                    <?php endforeach; ?>
                  </select>
                </span>
                ou outro: <input class="input" type="text" name="otherService" required />
              </p>
              
              <p class="control">
                <label class="label">Valor:</label>
                <input class="input" type="text" name="serviceValue" required  />
              </p>
              
              <hr />
              
              <p class="control login">
                <button class="button is-primary is-outlined is-large is-fullwidth" button="submit">Cadastrar</button>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</body>
</html>