<?php
include('./src/actions/redirectIfNotAuthenticated.php');
include('./src/actions/redirectIfNotProvider.php');
require_once("./src/actions/db.php");

$pageTitle = 'Gerenciamento de Serviços';

$userToken = $_COOKIE['userToken'];

$query = "SELECT userId FROM user WHERE userToken = '$userToken'" ;

$user = mysqli_fetch_array(mysqli_query($conn,$query));

include('./src/enums/status.php');
?>

<!DOCTYPE html>
<html lang="en">
<?php include("./src/components/header.php") ?>

<body>
  <section class="hero is-fullheight has-text-centered">
    <?php include("./src/components/navbar.php") ?>
    
    <div class="hero-body">
      <div class="container is-fluid">
        <h1 class="title">
          Gerenciamento de Serviços
        </h1>
        
        <?php
        $status = $serviceStatus["active"];
        $sql = "SELECT * FROM solicitation WHERE solicitationStatus = '$status' AND userToken = '$userToken' ORDER BY solicitationDate DESC";
        $active = mysqli_query($conn,$sql);
        ?>
        <?php while($row = mysqli_fetch_array($active)) { ?>
          <?php
          $userId = $row['userId'];
          $query = "SELECT userName FROM user WHERE userToken = '$userId'" ;
          $user = mysqli_fetch_array(mysqli_query($conn,$query));
          ?>
          
          <?php echo $row['solicitationId']; ?>
          <?php echo $row['solicitationDate']; ?>
          <?php echo $user['userName'] ?>
          <?php } ?>
          <?php
          $status = $serviceStatus["pending"];
          $sql = "SELECT * FROM solicitation WHERE solicitationStatus = '$status' AND userToken = '$userToken' ORDER BY solicitationDate DESC";
          $pending = mysqli_query($conn,$sql);
          ?>
          <?php while($row = mysqli_fetch_array($pending)) { ?>
            <?php
            $userId = $row['userId'];
            $query = "SELECT userName FROM user WHERE userToken = '$userId'" ;
            $user = mysqli_fetch_array(mysqli_query($conn,$query));
            ?>
            
            <?php echo $row['solicitationId']; ?>
            <?php echo $row['solicitationDate']; ?>
            <?php echo $user['userName'] ?>
            <?php } ?>
            
            <?php if(count(mysqli_fetch_array($active)) < 3) { ?>
              <?php
              $status = $serviceStatus["request"];
              $sql = "SELECT * FROM solicitation WHERE solicitationStatus = '$status' AND userToken = '$userToken' ORDER BY solicitationDate DESC";
              $request = mysqli_query($conn,$sql);
              ?>
              
              <?php while($row = mysqli_fetch_array($request)) { ?>
                <?php
                $userId = $row['userId'];
                $query = "SELECT userName FROM user WHERE userToken = '$userId'" ;
                $user = mysqli_fetch_array(mysqli_query($conn,$query));
                ?>
                
                <?php echo $row['solicitationId']; ?>
                <?php echo $row['solicitationDate']; ?>
                <?php echo $user['userName'] ?>
                <?php } ?>
                <?php } ?>
                <?php
                $status = $serviceStatus["done"];
                $sql = "SELECT * FROM solicitation WHERE solicitationStatus = '$status' AND userToken = '$userToken' ORDER BY solicitationDate DESC";
                $done = mysqli_query($conn,$sql);
                ?>
                
                <?php while($row = mysqli_fetch_array($done)) { ?>
                  <?php
                  $userId = $row['userId'];
                  $query = "SELECT userName FROM user WHERE userToken = '$userId'" ;
                  $user = mysqli_fetch_array(mysqli_query($conn,$query));
                  ?>
                  
                  <?php echo $row['solicitationId']; ?>
                  <?php echo $row['solicitationDate']; ?>
                  <?php echo $user['userName'] ?>
                  <?php } ?>
                </div>
              </div>
            </div>
          </section>
        </body>
        </html>