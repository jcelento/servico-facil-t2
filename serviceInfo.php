<?php
include('./src/actions/redirectIfNotAuthenticated.php');
include('./src/actions/redirectIfNotUser.php');
require_once("./src/actions/db.php");

// Trocar para userId
$userToken = $_COOKIE['userToken'];

$sql = "SELECT * FROM user WHERE userToken = '$userToken'";
$user = mysqli_fetch_array(mysqli_query($conn,$sql));

if(!$user) {
  include('./src/actions/logout.php');
}

$pageTitle = 'Informações de Serviço';

$serviceId =  $_GET['serviceId'];

$sql = "SELECT * FROM service WHERE serviceId = '$serviceId'";
$service = mysqli_fetch_array(mysqli_query($conn,$sql));

if(!$service) {
  echo "Algo aconteceu";
  exit;
}

$userId = $service['userId'];

$sql = "SELECT * FROM user WHERE userId = '$userId'";
$provider = mysqli_fetch_array(mysqli_query($conn,$sql));

$userCreditAfter = ((float)$user['userCredit'] - (float)$service['serviceValue']);
?>

<!DOCTYPE html>
<html lang="en">
<?php include("./src/components/header.php") ?>

<body>
  <section class="hero is-fullheight has-text-centered">
    <?php include("./src/components/navbar.php") ?>
    
    <div class="hero-body">
      <div class="container is-fluid">
        <section class="hero is-primary">
          <div class="hero-body">
            <div class="container is-fluid">
              <h1 class="title">
                Informações sobre o serviço
              </h1>
            </div>
          </div>
        </section>
        
        <br />
        
        <div class="container">
          <div class="columns">
            <div class="column is-8 is-offset-2">
              <form class="field is-grouped" method="post" action="./src/actions/serviceInfo.php">
                <input hidden name="providerId" value=<?php echo $userId; ?> />
                <input hidden name="serviceId" value=<?php echo $serviceId; ?> />
                <input hidden name="userToken" value=<?php echo $userToken; ?> />
                
                <p class="control">
                  <label class="label title is-3">Nome:</label>
                  <span>
                    <?php echo $service['serviceName']; ?>                    
                  </span>
                </p>
                
                <p class="control">
                  <label class="label title is-3">Tipo:</label>
                  <span>
                    <?php echo $service['serviceType']; ?>
                  </span>
                </p>
                
                <p class="control">
                  <label class="label title is-3">Prestador:</label>
                  <span>
                    <?php echo $provider['userName']; ?>
                  </span>
                </p>
                
                <p class="control">
                  <label class="label title is-3">Preço (em créditos):</label>
                  <span>
                    <?php echo $service['serviceValue']; ?>
                  </span>
                  
                  <label class="label title is-3">Você possui: </label>
                  <span>
                    <?php echo $user['userCredit']; ?>
                  </span>
                  
                  <label class="label title is-3">Você ficará com: </label>
                  <span>
                    <?php echo $userCreditAfter ?>
                  </span>
                </p>
                
                <p class="control">
                  <?php if($userCreditAfter > 0) : ?>
                    <button class="button is-primary is-outlined is-large is-fullwidth" button="submit">Contratar</button>
                  <?php else : ?>
                    <label class="label title is-4">Você não pode contratar esse serviço</label>
                  <?php endif; ?>                
                </p>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</body>
</html>