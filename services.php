<?php
include('./src/actions/redirectIfNotAuthenticated.php');
include('./src/actions/redirectIfNotUser.php');
$pageTitle = 'Página de Serviços';

require_once("./src/actions/db.php");
require_once("./src/actions/get_types.php");
?>

<!DOCTYPE html>
<html lang="en">
<?php include("./src/components/header.php") ?>

<body>
  <section class="hero is-fullheight has-text-centered">
    <?php include("./src/components/navbar.php") ?>
    
    <div class="hero-body">
      <div class="container is-fluid">
        <h1 class="title">
          Serviços
        </h1>
        
        <div class="services">
          <form class="field is-grouped" method="get">
            <p class="control">
              <label class="label">Perfil:</label>
              <span class="select is-fullwidth">
                <select name="choosedService" required>
                  <option disabled selected>Selecione Um serviço</option>
                  <?php foreach($services as $value): ?>
                    <option value=<?php echo $value ?>> <?php echo $value ?> </option>
                  <?php endforeach; ?>
                </select>
              </span>
            </p>
            
            <p class="control">
              <button class="button is-primary is-outlined is-large is-fullwidth"  type="submit" name="submit" button="submit">Pesquisar</button>
            </p>
          </form>
        </div>

        <?php
        if(!empty($_GET['choosedService'])){
          $serviceType = $_GET['choosedService'];
          
          $sql = "SELECT * FROM service WHERE serviceType = '$serviceType' ORDER BY userId DESC";
          $result = mysqli_query($conn,$sql);
          
          if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
          }
          
          if (empty($result)) {
            echo "Está vazio essa porra";
            exit;
          }
          ?>

          <div class="container-fluid">
            <table border="0" class=" table table-hover table-striped ">
              <thead>
                <tr>
                  <td>Serviço</td>  
                  <td>Tipo</td>
                  <td>Valor</td>
                  <td>Solicitar</td>
                </tr>
              </thead>
              <tbody>
                <?php while($row = mysqli_fetch_array($result)) { ?>
                  <tr>
                    <td><?php echo $row['serviceName']; ?></td>
                    <td><?php echo $row['serviceType']; ?></td>
                    <td><?php echo $row['serviceValue']; ?></td>
                    <td><a href="serviceInfo.php?serviceId=<?php echo $row["serviceId"]; ?>" class="link"><img alt='Solicitar' title='Solicitar' src='images/request.png' width='20px' height='20px' hspace='10' /></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </section>
</body>
</html>