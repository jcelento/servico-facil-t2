CREATE DATABASE IF NOT EXISTS servicoFacil;

CREATE TABLE IF NOT EXISTS servicoFacil.user (
  `userId` int(8) NOT NULL,
  `userToken` varchar(90) NOT NULL,
  `userName` varchar(55) NOT NULL,
  `userEmail` varchar(90) NOT NULL,
  `userPassword` varchar(55) NOT NULL,
  `userProfile` varchar(9) NOT NULL,
  `userCredit` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

ALTER TABLE servicoFacil.user
 ADD PRIMARY KEY (`userId`);

ALTER TABLE servicoFacil.user
MODIFY `userId` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS servicoFacil.service(
  `serviceId` int(8) NOT NULL,
  `serviceType` varchar(90) NOT NULL,
  `serviceName` varchar(55) NOT NULL,
  `serviceValue` float NOT NULL,
  `userId` int(8) 
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

ALTER TABLE servicoFacil.service
 ADD PRIMARY KEY (`serviceId`),
 ADD FOREIGN KEY (`userId`) REFERENCES user(`userId`);

ALTER TABLE servicoFacil.service
MODIFY `serviceId` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS servicoFacil.solicitation(
  `solicitationId` int(8) NOT NULL,
  `solicitationDate` varchar(10) NOT NULL,
  `solicitationStatus` int(8) NOT NULL DEFAULT '0',
   `userToken` varchar(90) NOT NULL,
   `userId` int(8) NOT NULL,
   `serviceId` int(8) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

ALTER TABLE servicoFacil.solicitation
 ADD PRIMARY KEY (`solicitationId`),
 ADD FOREIGN KEY (`userId`) REFERENCES user(`userId`),
 ADD FOREIGN KEY (`userToken`) REFERENCES user(`userToken`);

ALTER TABLE servicoFacil.solicitation
MODIFY `solicitationId` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS servicoFacil.rating(
  `ratingId` int(8) NOT NULL,
  `ratingGrade` int(8) NOT NULL,
  `ratingDescription` varchar(55) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

ALTER TABLE servicoFacil.rating
 ADD PRIMARY KEY (`ratingId`),
 ADD FOREIGN KEY (`userId`) REFERENCES user(`userId`),
 ADD FOREIGN KEY (`userToken`) REFERENCES user(`userToken`);
;

ALTER TABLE servicoFacil.rating
MODIFY `ratingId` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;
